# Projekt-Readme

## Einführung

Dieses Projekt enthält verschiedene Anleitungen und Konfigurationsdateien für die Automatisierung von IT-Infrastrukturen. Es ist in zwei Hauptordner unterteilt: `debian-install` und `ansible`.

## Ordnerstruktur

- `debian-install/`: Enthält Anleitungen und Konfigurationsdateien für die automatische Installation von Debian 12 mit einem USB-Stick und einer Preseed-Datei.
  - [Readme für Debian-Installation](debian-install/README.md)

- `ansible/`: Enthält Anleitungen und Konfigurationsdateien für die Automatisierung mit Ansible für die Installtion von PROXMOX.
  - [Readme für Ansible](ansible/README.md)

## Weitere Informationen

Für weitere Details, öffnen Sie die Readme-Dateien in den jeweiligen Unterordnern.