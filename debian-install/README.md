# Debian 12 Autoinstallation mit Preseeding

Dieses Projekt beschreibt, wie man eine automatische Installation von Debian 12 mit Preseeding durchführt. Es enthält die Konfigurationsdateien `preseed.cfg` und `grub.cfg`.

## Voraussetzungen

- Ein USB-Stick mit mindestens 8 GB Speicherplatz
- Ein Computer, auf dem Debian 12 installiert werden soll
  - Gute Konfiguration: 16 CPU-Kerne, 64 GB RAM, 2 TB Festplattenspeicher
- Zugang zu einem Debian 12 ISO-Image

## Dateien

### preseed.cfg

Die `preseed.cfg`-Datei enthält alle notwendigen Einstellungen für die automatische Installation. Sie konfiguriert unter anderem:

- Lokalisierung und Tastaturlayout
- Netzwerkeinstellungen
- Spiegelserver
- Festplatten und Partitionierung
- Benutzer und Passwörter
- Paketauswahl
- Bootloader

### Variablen

Sie können die folgenden Variablen in der `preseed.cfg`-Datei anpassen:

- Netzwerkeinstellungen:
  - `d-i netcfg/get_ipaddress string 192.168.1.230`
  - `d-i netcfg/get_netmask string 255.255.255.0`
  - `d-i netcfg/get_gateway string 192.168.1.1`
  - `d-i netcfg/get_nameservers string 192.168.1.2`

- Festplattenauswahl:
  - `d-i partman-auto/disk string /dev/nvme0n1`

- Verschlüsseltes Benutzerpasswort:
  - `d-i passwd/user-password-crypted password $1$5RDfhut2$DuRgFVinFiLqvyPpQLut/1` (Standard: `Test123!`)

- SSH-Schlüssel für den Benutzer `dev`:
  - `in-target /bin/sh -c "echo 'ssh-rsa-key' > /home/dev/.ssh/authorized_keys"; \`

## Partitionierung

Die Partitionierung ist in der `preseed.cfg`-Datei unter dem Abschnitt `d-i partman-auto/expert_recipe string` definiert. Sie können diese Einstellungen an Ihre Bedürfnisse anpassen.

### Default-Werte

- EFI-Partition: 1024 MB
- Swap-Partition: 8192 MB
- Root-Partition: Rest des verfügbaren Speicherplatzes

### grub.cfg

Die `grub.cfg`-Datei enthält die Bootloader-Einstellungen und sorgt dafür, dass die `preseed.cfg` während der Installation verwendet wird.

## Anleitung

### Unter Windows

1. Laden Sie das Debian 12 ISO-Image und das Tool Rufus herunter.
2. Öffnen Sie Rufus und wählen Sie das ISO-Image und den USB-Stick aus.
3. Klicken Sie auf "Start", um den USB-Stick zu erstellen.
4. Nachdem der Stick erstellt wurde, kopieren Sie die `preseed.cfg` Datei in das Root-Verzeichnis und die `grub.cfg` unter /boot/grub/ des USB-Sticks.

### Linux USB-Stick vorbereiten 

1. Laden Sie das Debian 12 ISO-Image herunter.
2. Stecken Sie den USB-Stick in den Computer.
3. Identifizieren Sie den Gerätenamen des USB-Sticks mit dem Befehl `lsblk` oder `fdisk -l`.
4. Schreiben Sie das ISO-Image auf den USB-Stick. **Achtung: Dies löscht alle Daten auf dem Stick!**

    ```bash
    sudo dd if=/pfad/zum/debian-12.iso of=/dev/sdX bs=4M; sync
    ```
    Ersetzen Sie `/pfad/zum/debian-12.iso` durch den tatsächlichen Pfad zum ISO-Image und `/dev/sdX` durch den Gerätenamen des USB-Sticks.

5. Mounten Sie die erste Partition des USB-Sticks:

    ```bash
    sudo mount /dev/sdX1 /mnt
    ```

6. Kopieren Sie die `preseed.cfg` und `grub.cfg` in das Root-Verzeichnis des USB-Sticks:

    ```bash
    sudo cp preseed.cfg /mnt/
    sudo cp grub.cfg /mnt/boot/grub/
    ```

7. Unmounten Sie den USB-Stick:

    ```bash
    sudo umount /mnt
    ```

Jetzt ist der USB-Stick bereit für die Installation.

## Durchführung der Installation

1. Stecken Sie den USB-Stick in den Zielcomputer.
2. Booten Sie den Computer von dem USB-Stick.
3. Wählen Sie im GRUB-Menü die Option für die automatische Installation.

Die Installation sollte nun automatisch ablaufen.

## Lizenz

Dieses Projekt ist unter der MIT-Lizenz veröffentlicht. Siehe `LICENSE` für weitere Informationen.