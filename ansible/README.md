# Ansible Playbook für die Installation und Konfiguration von Proxmox VE auf Debian Bookworm

Dieses Ansible-Projekt automatisiert die Installation und Grundkonfiguration von Proxmox VE auf einem Debian Bookworm-System.

## Voraussetzungen

- Ein oder mehrere Server mit Debian Bookworm
- Ansible installiert 
- SSH-Zugriff auf den Zielserver mit einem Benutzer, der `sudo` Berechtigungen hat

## Struktur des Projekts

- `inventory.ini`: Inventardatei mit den Zielservern
- `vault.yml`: Verschlüsselte Datei mit sensiblen Daten wie Passwörtern
- `ansible.cfg`: Globale Einstellungen für Ansible
- `adhoc-befehle`: Datei für Ad-Hoc-Befehle
- `ansible.log`: Log-Datei für Ansible-Ausgaben
- `roles/`: Verzeichnis für wiederverwendbare Rollen
  - `proxmox-install`: Rolle für die Installation von Proxmox
    - `handlers/main.yml`: Handler für diese Rolle
    - `tasks/main.yml`: Haupt-Playbook für die Installation von Proxmox VE
    - `templates/network_interfaces.j2`: Netzwerkkonfigurations-Template
- `site.yml`: Übergeordnetes Playbook (falls verwendet)
- `vars.yml`: Zentrale Datei für Variablen
- `README.md`: Diese Anleitung

## Variablen

- `my_hostname`: Hostname des Servers
- `my_domain`: Domain des Servers
- `admin_user`: Benutzername des Proxmox Admin-Accounts
- `gateway`: Gateway-IP-Adresse
- `proxmox_ip`: IP-Adresse für Proxmox
- Weitere Variablen können in `vars.yml` und `vault.yml` definiert werden.

## Anleitung

1. Klonen Sie das Repository:
    ```bash
    git clone <repository-url>
    ```

2. Wechseln Sie in das Projektverzeichnis:
    ```bash
    cd <project-directory>
    ```

3. Passen Sie die `inventory.ini`, `vars.yml` und `vault.yml` nach Ihren Bedürfnissen an.

4. Führen Sie das Playbook mit dem Vault-Passwort aus:
    ```bash
    ansible-playbook site.yml --ask-vault-pass
    ```

## Aufgaben (Tasks)

- Netzwerkkonfiguration
- Festplattenformatierung und -einbindung
- Hinzufügen des Proxmox Repositories und Installation der erforderlichen Pakete
- Entfernen des Debian Standard-Kernels und Aktualisieren von Grub
- Erstellung eines Admin-Benutzers für Proxmox und Zuweisung von Berechtigungen
- Neustart des Systems

Für detaillierte Informationen zu den einzelnen Aufgaben siehe `roles/proxmox-install/tasks/main.yml`.

## Dateistruktur nach der Einrichtung

Ihre Projektstruktur sollte in etwa so aussehen:

```
├── README.md
├── adhoc-befehle
├── ansible.cfg
├── inventory.ini
├── roles
│   └── proxmox-install
│       ├── handlers
│       │   └── main.yml
│       ├── tasks
│       │   └── main.yml
│       └── templates
│           └── network_interfaces.j2
├── site.yml
├── vars.yml
└── vault.yml
```

## Beispiel für `vars.yml`

```yaml
---
# Variablen für Proxmox Installation
my_hostname: "proxmox"     
my_domain: "example.de"   
admin_user: "admin"    
gateway: 192.168.1.1      
proxmox_ip: 192.168.1.230
```

## Beispiel für `vault.yml`

```yaml
---
become_pass: "Test123!"
vault_admin_password: "Test123!"
```